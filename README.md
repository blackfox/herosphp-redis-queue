# herosphp/redis-queue

redis-queue

## install
```shell
    composer install herosphp/redis-queue
```

## usage

### publish queue.config.php
```shell
    composer vendor:publish "herosphp/redis-queue"
```

### config/process.config.php
```php
return [
    'redis-queue' => [
        'enable' => true,
        'handler' => Consumer::class,
        'count' => 1,
        'constructor' => [
            'consumer_dir' => APP_PATH.'queue',
        ],
    ],
];
```

### app/queue/DemoQueue.php
```php
<?php
declare(strict_types=1);

namespace app\queue;

use herosphp\plugin\queue\ConsumerInterface;

class DemoQueue implements ConsumerInterface
{
    //要消费的队列名
    public string $queue = 'demo';

    // 连接名，对应 config/redis_queue.php 里的连接`
    public string $connection = 'default';

    public function consume(array $data): void
    {
        echo 'queue'.json_encode($data).PHP_EOL;
    }
}

```

### client
```php
Client::send('demo', [1,2,3]); or Client::send('demo', [1,2,3],5); // delay 5s
```


