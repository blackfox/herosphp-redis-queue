<?php
declare(strict_types=1);

namespace herosphp\plugin\queue;

interface ConsumerInterface
{
    public function consume(array $data): void;
}
